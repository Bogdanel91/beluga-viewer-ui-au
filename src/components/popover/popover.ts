import { CommonHelper } from './../../helpers/common-helpers';
import { customElement } from "aurelia-templating";
import { bindable, inject, TaskQueue, bindingMode } from "aurelia-framework";
import { ANIMATION_TIME } from 'misc/constants';

const COMPONENT_NAME = 'blgz-popover';

@customElement(COMPONENT_NAME)
@inject(Element, TaskQueue)
export class PopoverCustomElement {
  @bindable({ defaultBindingMode: bindingMode.twoWay }) isVisible: boolean = false;
  @bindable({ defaultBindingMode: bindingMode.oneTime }) onVisibilityChange: (params?: any) => void;
  _isVisible: boolean = false;
  _isVisibleAnim: boolean = false;
  uuid: string = [COMPONENT_NAME, '-', CommonHelper.guid()].join('');
  timeoutId: number;
  popoverElement: Element;
  $popoverElement: JQuery;
  $anchorElement: JQuery;
  $window: JQuery;

  constructor(private readonly element: Element, private readonly taskQueue: TaskQueue) { }

  attached() {
    this.$window = $(window);
    this.$popoverElement = $(this.popoverElement);
    this.$anchorElement = $(this.element).prev();
  }

  bind() {

  }

  isVisibleChanged(isVisible: boolean) {
    const resizeEventId = `resize.${this.uuid}`;
    const clickEventId = `click.${this.uuid}`;

    isVisible ? this.uiShow() : this.uiHide();

    this.taskQueue.queueTask(() => {
      if (isVisible) {
        this.$window.on(resizeEventId, this.onResize.bind(this));
        this.$window.on(clickEventId, this.onGlobalClick.bind(this));
      } else {
        this.$window.off(resizeEventId);
        this.$window.off(clickEventId);
      }
    });

    this.onVisibilityChange && this.onVisibilityChange({
      isVisible: this.isVisible
    });
  }

  private applyCoords() {
    const anchorOffsets = this.$anchorElement.offset();
    const anchorHeight = this.$anchorElement.outerHeight();

    this.$popoverElement.css({
      top: anchorHeight + anchorOffsets.top,
      left: anchorOffsets.left
    });
  }

  private uiShow() {
    clearTimeout(this.timeoutId);
    this.applyCoords();
    this._isVisible = true;
    this.timeoutId = window.setTimeout(() => {
      this._isVisibleAnim = true;
    }, 1);
  }

  private uiHide() {
    clearTimeout(this.timeoutId);
    this._isVisibleAnim = false;
    this.timeoutId = window.setTimeout(() => {
      this._isVisible = false;
    }, ANIMATION_TIME);
  }

  private onResize() {
    this.applyCoords();
  }

  private onGlobalClick(event: JQuery.Event) {
    const $target = $(event.target);

    if (!$target.closest(this.popoverElement).length) {
      this.isVisible = false;
    }
  }
}
