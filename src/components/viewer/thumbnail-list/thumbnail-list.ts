import { ComponentHelpers } from './../../../helpers/component-helpers';
import { bindable } from 'aurelia-framework';
import { customElement } from 'aurelia-templating';
import { UIPage, ThumbnailSelectedData } from 'components/viewer/thumbnail-list/thumbnail-list-types';

const COMPONENT_NAME = 'blgz-viewer-thumbnail-list';
const EVENT_SELECTED = [COMPONENT_NAME, '-selected'].join('');

@customElement('blgz-viewer-thumbnail-list')
export class ViewerThumbnailListCustomElement {
  @bindable pages: Array<UIPage> = [];

  onSelfSelect(event: Event, page: UIPage) {
    for (const pg of this.pages) {
      pg.isSelected = false;
    }

    page.isSelected = true;

    if (event.srcElement) {
      const eventData: ThumbnailSelectedData = {
        page,
        element: event.srcElement
      };
      ComponentHelpers.dispatch(event.srcElement, EVENT_SELECTED, eventData);
    }
  }
}
