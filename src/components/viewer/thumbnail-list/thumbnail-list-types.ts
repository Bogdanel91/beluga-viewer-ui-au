import { PageDto } from './../page/page-dto';

export interface UIPage extends PageDto {
  isSelected: boolean;
}

export interface ThumbnailSelectedData {
  page: UIPage;
  element: Element;
}
