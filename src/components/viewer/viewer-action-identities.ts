import { ActionIdentity, ActionIdentityData, ActionToggleGroup } from "components/ribbon/action/action-types";


export class ViewerActionIdentities {
  ZoomIn: ActionIdentityData = {
    identity: ActionIdentity.ZoomIn
  }

  ZoomOut: ActionIdentityData = {
    identity: ActionIdentity.ZoomOut
  }

  ResetZoom: ActionIdentityData = {
    identity: ActionIdentity.ResetZoom
  }

  DrawRectangle: ActionIdentityData = {
    identity: ActionIdentity.DrawRectangle,
    toggleGroup: ActionToggleGroup.Drawing
  }

  DrawEllipse: ActionIdentityData = {
    identity: ActionIdentity.DrawEllipse,
    toggleGroup: ActionToggleGroup.Drawing
  }

  DrawTextarea: ActionIdentityData = {
    identity: ActionIdentity.DrawTextarea,
    toggleGroup: ActionToggleGroup.Drawing
  }

  DrawFree: ActionIdentityData = {
    identity: ActionIdentity.DrawFree,
    toggleGroup: ActionToggleGroup.Drawing
  }

  EraseFree: ActionIdentityData = {
    identity: ActionIdentity.EraseFree,
    toggleGroup: ActionToggleGroup.Drawing
  }

  ColorAndThickness: ActionIdentityData = {
    identity: ActionIdentity.ColorAndThickness
  }
}
