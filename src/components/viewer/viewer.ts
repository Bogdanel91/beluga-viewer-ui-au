import { ActionIdentity } from 'components/ribbon/action/action-types';
import { RibbonActionCutomElement } from './../ribbon/action/action';
import { ViewerActionIdentities } from './viewer-action-identities';
import { DomHelpers } from './../../helpers/dom-helpers';
import { bindable, children, inject } from 'aurelia-framework';
import { customElement } from "aurelia-templating";
import { PageDto } from 'components/viewer/page/page-dto';
import { ThumbnailSelectedData } from 'components/viewer/thumbnail-list/thumbnail-list-types';
import { ViewerState } from 'components/viewer/viewer-state';
import { ViewerActionCallbacks } from 'components/viewer/viewer-action-callbacks';
import { Color } from 'components/color-picker/color-picker-types';
import { ColorPickerSelectRibbonCustomElement } from 'components/ribbon/color-picker-select-ribbon/color-picker-select-ribbon';
import { ViewerPageCustomElement } from './page/page';

@customElement('blgz-viewer')
@inject(ViewerState)
export class ViewerCustomElement {
  @bindable pages: Array<PageDto> = [];
  @children('[as-element="blgz-ribbon-action"]') actions: Array<RibbonActionCutomElement> = [];
  @children('[as-element="blgz-color-picker-select-ribbon"]') colorPickers: Array<ColorPickerSelectRibbonCustomElement> = [];
  pagesContainerElement: Element;
  actionIdentities: ViewerActionIdentities = new ViewerActionIdentities();
  actionCallbacks: ViewerActionCallbacks = new ViewerActionCallbacks(this);
  defaultBorderColor: Color = {
    name: 'white',
    code: '#34eabf'
  };
  defaultFillColor: Color = {
    name: 'white',
    code: '#456eac'
  };
  pagesVMs: Array<ViewerPageCustomElement> = [];

  constructor(
    private viewerState: ViewerState
  ) {
    this.viewerState.currentColors = {
      fill: this.defaultFillColor,
      border: this.defaultBorderColor
    }
  }

  get drawingMode() {
    if (this.viewerState.drawingMode) {
      if (this.viewerState.drawingMode === ActionIdentity.EraseFree) {
        return 'blgz-viewer__pagesInnerContainer--inDrawingModeEraser';
      } else {
        return 'blgz-viewer__pagesInnerContainer--inDrawingMode';
      }
    }
  }

  loadImages(pages: Array<PageDto>) {
    this.pages = pages;
  }

  getShapes() {
    const len = this.pagesVMs.length;
    const toret = [];

    for (let index = 0; index < len; index++) {
      const pageVM = this.pagesVMs[index];

      toret.push({
        index: index,
        shapes: pageVM.getShapes()
      });
    }

    console.log("getShapes", toret);

    return toret;
  }

  onSelfMousedown() {
    for (const colPicker of this.colorPickers) {
      colPicker.hide();
    }

    return true;
  }

  onBorderColorSelect(color: Color) {
    console.log(color);
    this.viewerState.currentColors.border = color;
  }

  onFillColorSelect(color: Color) {
    console.log(color);
    this.viewerState.currentColors.fill = color;
  }

  onThumbnailListSelected(eventData: ThumbnailSelectedData) {
    const element = document.getElementById(eventData.page.guid);

    if (element) {
      DomHelpers.scrollTo(element, this.pagesContainerElement);
    }
  }

  onRibbonActionClicked(action: RibbonActionCutomElement) {
    const identity = action.identityData.identity;
    const toggleGroup = action.identityData.toggleGroup;

    if (toggleGroup) {
      for (const actionItem of this.actions) {
        const sameGroup = actionItem.identityData.toggleGroup === toggleGroup;
        const notSelf = actionItem.identityData.identity !== identity;

        if (sameGroup && notSelf) {
          actionItem.isActive = false;
        }
      }

      action.isActive = !action.isActive;

      if (
        identity === ActionIdentity.DrawFree ||
        identity === ActionIdentity.DrawRectangle ||
        identity === ActionIdentity.DrawEllipse ||
        identity === ActionIdentity.DrawTextarea ||
        identity === ActionIdentity.EraseFree
      ) {
        this.viewerState.drawingMode = action.isActive ? identity : null;
      }
    }
  }
}
