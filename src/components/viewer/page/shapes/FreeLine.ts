import { ViewerPageCustomElement } from 'components/viewer/page/page';
import { BaseDrawing } from "components/viewer/page/shapes/_base";
import { ViewerState } from 'components/viewer/viewer-state';
import * as Konva from 'konva';
import { ActionIdentity } from 'components/ribbon/action/action-types';

export class FreeLine implements BaseDrawing {
  page: ViewerPageCustomElement;
  lineCoords: Array<number> = [];

  constructor(
    page: ViewerPageCustomElement,
    private readonly viewerState: ViewerState
  ) {
    this.page = page;
    this.lineCoords.push(this.page.startingPointerPosition.x);
    this.lineCoords.push(this.page.startingPointerPosition.y);
  }

  liveDraw() {
    const start = this.page.startingPointerPosition;
    const curr = this.page.stage.getPointerPosition();

    this.lineCoords.push(curr.x);
    this.lineCoords.push(curr.y);

    const newLine = new Konva.Line({
      points: [start.x, start.y, curr.x, curr.y],
      stroke: this.viewerState.currentColors.fill.code,
      strokeWidth: this.viewerState.drawingThickness,
      lineCap: 'round',
      lineJoin: 'round'
    });

    if (this.viewerState.drawingMode === ActionIdentity.DrawFree) {
      (newLine as any).globalCompositeOperation('source-over');
    } else {
      (newLine as any).globalCompositeOperation('destination-out');
    }

    this.page.drawingLayer.add(newLine);

    this.page.startingPointerPosition = {
      x: curr.x,
      y: curr.y
    };
  }

  endDraw() {
    this.page.drawingLayer.destroyChildren();
    this.page.drawingLayer.draw();
    const newLine = new Konva.Line({
      points: this.lineCoords.slice(0),
      stroke: this.viewerState.currentColors.fill.code,
      strokeWidth: this.viewerState.drawingThickness,
      lineCap: 'round',
      lineJoin: 'round'
    });

    // todo[burs]: remove any after the .d.ts of konva is updated
    if (this.viewerState.drawingMode === ActionIdentity.DrawFree) {
      (newLine as any).globalCompositeOperation('source-over');
    } else {
      (newLine as any).globalCompositeOperation('destination-out');
    }

    this.page.paintedLayer.add(newLine);
  }
}
