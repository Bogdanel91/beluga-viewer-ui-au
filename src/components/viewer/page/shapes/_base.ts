import { ViewerPageCustomElement } from "components/viewer/page/page";

export interface BaseDrawing {
  page: ViewerPageCustomElement;
  liveDraw: () => void;
  endDraw: () => void;
}
