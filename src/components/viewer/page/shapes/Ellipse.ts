import { BaseDrawing } from './_base';
import * as Konva from 'konva';
import { ViewerState } from 'components/viewer/viewer-state';
import { ViewerPageCustomElement } from 'components/viewer/page/page';

export class Ellipse implements BaseDrawing {
  coords: Konva.EllipseConfig;

  constructor(
    public page: ViewerPageCustomElement,
    private readonly viewerState: ViewerState
  ) { }

  liveDraw() {
    const start = this.page.startingPointerPosition;
    const curr = this.page.stage.getPointerPosition();

    const radiusX = (curr.x - start.x) + 5;
    const radiusY = (curr.y - start.y) + 5;

    this.page.drawingLayer.destroyChildren();

    if (radiusX < 0) {
      return;
    }

    // todo[burs]: revise this
    const coords: Konva.EllipseConfig = {
      x: Math.max(start.x, curr.x) - Math.abs(start.x - curr.x) / 2,
      y: Math.max(start.y, curr.y) - Math.abs(start.y - curr.y) / 2,
      radius: {
        x: radiusX,
        y: radiusY
      }
    };

    const newEllipse = new Konva.Ellipse({
      x: coords.x,
      y: coords.y,
      radius: coords.radius,
      fill: this.viewerState.currentColors.fill.code,
      stroke: this.viewerState.currentColors.border.code,
      strokeWidth: 3,
    });

    this.page.drawingLayer.add(newEllipse);

    this.coords = Object.assign({}, this.coords, coords);
  }

  endDraw() {
    this.page.drawingLayer.destroyChildren();
    this.page.drawingLayer.draw();

    const coords: Konva.EllipseConfig = this.coords;
    const newEllipse = new Konva.Ellipse({
      x: coords.x,
      y: coords.y,
      radius: coords.radius,
      fill: this.viewerState.currentColors.fill.code,
      stroke: this.viewerState.currentColors.border.code,
      strokeWidth: 3
    });

    this.page.paintedLayer.add(newEllipse);
  }
}
