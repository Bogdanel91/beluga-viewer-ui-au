import { BaseDrawing } from 'components/viewer/page/shapes/_base';
import { ViewerPageCustomElement } from 'components/viewer/page/page';
import { ViewerState } from 'components/viewer/viewer-state';
import * as Konva from 'konva';

export class Rectangle implements BaseDrawing {
  coords: Konva.RectConfig;

  constructor(
    public page: ViewerPageCustomElement,
    private readonly viewerState: ViewerState
  ) {

  }

  liveDraw() {
    const start = this.page.startingPointerPosition;
    const curr = this.page.stage.getPointerPosition();

    this.page.drawingLayer.destroyChildren();

    const coords: Konva.RectConfig = {
      x: start.x,
      y: start.y,
      width: curr.x - start.x,
      height: curr.y - start.y
    };

    const newRect = new Konva.Rect({
      x: coords.x,
      y: coords.y,
      width: coords.width,
      height: coords.height,
      fill: this.viewerState.currentColors.fill.code,
      stroke: this.viewerState.currentColors.border.code,
      strokeWidth: 3
    });

    this.page.drawingLayer.add(newRect);

    this.coords = Object.assign({}, this.coords, coords);
  }

  endDraw() {
    this.page.drawingLayer.destroyChildren();
    this.page.drawingLayer.draw();
    const coords: Konva.RectConfig = this.coords;
    const newRect = new Konva.Rect({
      x: coords.x,
      y: coords.y,
      width: coords.width,
      height: coords.height,
      fill: this.viewerState.currentColors.fill.code,
      stroke: this.viewerState.currentColors.border.code,
      strokeWidth: 3
    });

    this.page.paintedLayer.add(newRect);
  }
}
