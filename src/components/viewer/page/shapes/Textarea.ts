import { BaseDrawing } from 'components/viewer/page/shapes/_base';
import { ViewerPageCustomElement } from 'components/viewer/page/page';
import { ViewerState } from 'components/viewer/viewer-state';
import * as Konva from 'konva';
import { CommonHelper } from 'helpers/common-helpers';
import { PAGE_IGNORE_MOUSE_UP_ATTR } from 'components/viewer/page/page-dto';
import { Keyboard } from 'helpers/keyboard';

const TEXTAREA_GUID_ATTR = 'data-txtarea-identifier';

const TEXT_TOP_PAD: number = 2;
const TEXT_LEFT_PAD: number = 4;

export class Textarea implements BaseDrawing {
  coords: Konva.RectConfig;

  constructor(
    public page: ViewerPageCustomElement,
    private readonly viewerState: ViewerState
  ) {

  }

  liveDraw() {
    const start = this.page.startingPointerPosition;
    const curr = this.page.stage.getPointerPosition();

    this.page.drawingLayer.destroyChildren();

    const coords: Konva.RectConfig = {
      x: start.x,
      y: start.y,
      width: curr.x - start.x,
      height: curr.y - start.y
    };

    const newRect = new Konva.Rect({
      x: coords.x,
      y: coords.y,
      width: coords.width,
      height: coords.height,
      fill: this.viewerState.currentColors.fill.code,
      stroke: this.viewerState.currentColors.border.code,
      strokeWidth: 3
    });

    this.page.drawingLayer.add(newRect);

    this.coords = Object.assign({}, this.coords, coords);
  }

  endDraw() {
    this.page.drawingLayer.destroyChildren();
    this.page.drawingLayer.draw();

    const coords: Konva.RectConfig = this.coords;
    const newRect = new Konva.Rect({
      x: coords.x,
      y: coords.y,
      width: coords.width,
      height: coords.height,
      fill: this.viewerState.currentColors.fill.code,
      stroke: this.viewerState.currentColors.border.code,
      strokeWidth: 3
    });

    // todo[burs]: for some reason this gets messed up when the
    // width and height are negative. This is all because Konva apparently
    // does not know how to draw the text when the height and width are negative.
    // it knows how to drive the rect, so i guess it's an inconsistency in their implementation
    // need to further investigate
    const textCoords = newRect.getClientRect();

    const newText = new Konva.Text({
      x: textCoords.x ? textCoords.x + TEXT_LEFT_PAD : textCoords.x,
      y: textCoords.y ? textCoords.y + TEXT_TOP_PAD : textCoords.y,
      width: textCoords.width,
      height: textCoords.height,
      text: 'Simple Text',
      fontFamily: 'Calibri',
      fontSize: 30,
      fill: '#000'
    });

    this.page.paintedLayer.add(newRect);
    this.page.paintedLayer.add(newText);

    newText
      .on('dblclick', () => {
        this.buildTextarea(textCoords, newText);
      });

    this.buildTextarea(textCoords, newText);
  }

  private buildTextarea(shapeCoords: Konva.SizeConfig, textNode: Konva.Text) {
    if (!shapeCoords.width || !shapeCoords.height) {
      return;
    }

    const textarea = $('<textarea/>', {
      css: {
        'position': 'absolute',
        'top': shapeCoords.y,
        'left': shapeCoords.x,
        'width': Math.abs(shapeCoords.width),
        'height': Math.abs(shapeCoords.height),
        'z-index': 1223
      }
    });

    textarea
      .attr(PAGE_IGNORE_MOUSE_UP_ATTR, 'true')
      .attr(TEXTAREA_GUID_ATTR, CommonHelper.guid())
      .val(textNode.text());

    $(this.page.pageElement).append(textarea);

    this.attachTextareaEvents(textarea, textNode);
    this.focusTextarea(textarea);
  }

  private attachTextareaEvents(textarea: JQuery, textNode: Konva.Text) {
    const $textarea = textarea as any;

    $textarea
      .on('keydown', (event: JQueryKeyEventObject) => {
        const k = event.keyCode;

        if (k === Keyboard.Enter || k === Keyboard.Esc) {
          this.applyTextChanges(textarea, textNode);
        }

        return true;
      })
      .on('click', this.stopPropagation.bind(this))
      .on('mouseup', this.stopPropagation.bind(this))
      .on('mousedown', this.stopPropagation.bind(this));

    window.setTimeout(() => {
      const $body = $(document.body);
      const eventId = `txtarea-${$textarea.attr(TEXTAREA_GUID_ATTR)}`;

      $body.on(`click.${eventId}`, () => {
        this.applyTextChanges(textarea, textNode);
        $body.off(`click.${eventId}`);
      });
    }, 10);
  }

  private focusTextarea(textarea: JQuery, selectText: boolean = true) {
    window.setTimeout(() => {
      textarea.focus();

      if (selectText) {
        window.setTimeout(() => {
          textarea.select();
        }, 1);
      }
    }, 1);
  }

  private applyTextChanges(textarea: JQuery, textNode: Konva.Text) {
    const val = textarea.val();
    textNode.text(val ? val.toString() : '');
    this.page.paintedLayer.draw();
    $(document.body).off(`click.txtarea-${textarea.attr(TEXTAREA_GUID_ATTR)}`);
    textarea.remove();
  }

  private stopPropagation(event: JQueryEventObject) {
    event.stopImmediatePropagation();
  }
}
