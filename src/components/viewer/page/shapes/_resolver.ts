import { ActionIdentity } from 'components/ribbon/action/action-types';
import { ViewerState } from './../../viewer-state';
import { inject } from 'aurelia-framework';
import { BaseDrawing } from 'components/viewer/page/shapes/_base';
import { FreeLine } from 'components/viewer/page/shapes/FreeLine';
import { ViewerPageCustomElement } from 'components/viewer/page/page';
import { Ellipse } from 'components/viewer/page/shapes/Ellipse';
import { Rectangle } from 'components/viewer/page/shapes/Rectangle';
import { Textarea } from 'components/viewer/page/shapes/Textarea';

@inject(ViewerState)
export class DrawingFactory {
  constructor(private readonly viewerState: ViewerState) {

  }

  getDrawing(page: ViewerPageCustomElement): BaseDrawing | null {
    const state = this.viewerState.drawingMode;

    if (state === ActionIdentity.DrawFree) {
      return new FreeLine(page, this.viewerState);
    } else if (state === ActionIdentity.EraseFree) {
      return new FreeLine(page, this.viewerState);
    } else if (state === ActionIdentity.DrawEllipse) {
      return new Ellipse(page, this.viewerState);
    } else if (state === ActionIdentity.DrawRectangle) {
      return new Rectangle(page, this.viewerState);
    } else if (state === ActionIdentity.DrawTextarea) {
      return new Textarea(page, this.viewerState);
    } else {
      return null;
    }
  }
}
