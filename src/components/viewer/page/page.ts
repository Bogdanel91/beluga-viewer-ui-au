import { ViewerState } from './../viewer-state';
import { CommonHelper } from './../../../helpers/common-helpers';
import { PageDto, PAGE_IGNORE_MOUSE_UP_ATTR } from './page-dto';
import { bindable, TaskQueue, autoinject, bindingMode } from 'aurelia-framework';
import { customElement } from 'aurelia-templating';
import * as Konva from 'konva';
import { BaseDrawing } from 'components/viewer/page/shapes/_base';
import { DrawingFactory } from 'components/viewer/page/shapes/_resolver';

const MOUSE_MOVE_THRESHOLD: number = 2;

@customElement('blgz-viewer-page')
@autoinject()
export class ViewerPageCustomElement {
  @bindable({ defaultBindingMode: bindingMode.oneTime }) pageData: PageDto;
  uuid: string = CommonHelper.guid();
  pageElement: Element;
  imgElement: HTMLImageElement;
  width: number;
  height: number;
  stage: Konva.Stage;

  drawingLayer: Konva.FastLayer;
  paintedLayer: Konva.Layer;

  isDrawing: boolean;
  startingPointerPosition: Konva.Vector2d;

  currentDrawing: BaseDrawing;

  constructor(
    private readonly viewerState: ViewerState,
    private readonly taskQueue: TaskQueue,
    private readonly drawingFactory: DrawingFactory
  ) { }

  onSelfImgLoad() {
    this.taskQueue.queueMicroTask(() => {
      this.width = this.imgElement.naturalWidth;
      this.height = this.imgElement.naturalHeight;

      this.initStage();
    });
  }

  onSelfMousedown(event: MouseEvent) {
    if (!this.viewerState.drawingMode || this.eventStartedFromIgnorableElement(event)) {
      return true;
    }

    this.startingPointerPosition = this.stage.getPointerPosition();
    this.currentDrawing = this.drawingFactory.getDrawing(this)!;
    this.isDrawing = true;

    $(document).on(`mousemove.${this.uuid}`, `#${this.uuid}`, this.onSelfMouseMove.bind(this));
    $(document).on(`mouseup.${this.uuid}`, this.onSelfGlobalMouseUp.bind(this, event));
  }

  onSelfClick() {
    return true;
  }

  getShapes() {
    if (!(this.paintedLayer && typeof this.paintedLayer.getChildren === "function")) {
      return [];
    }

    const toret = [] as any[];

    this.paintedLayer.getChildren().each(knvNode => {
      toret.push(knvNode.toObject());
    });

    return toret;
  }

  private initStage() {
    this.stage = new Konva.Stage({
      container: this.uuid,
      width: this.width,
      height: this.height
    });

    this.drawingLayer = new Konva.FastLayer();
    this.stage.add(this.drawingLayer);

    this.paintedLayer = new Konva.Layer();
    this.stage.add(this.paintedLayer);

    this.stage.draw();
  }

  private onSelfMouseMove() {
    console.log('on MouseMove');
    if (!this.isDrawing) {
      return true;
    }

    this.currentDrawing.liveDraw();
    this.drawingLayer.draw();
  }

  private onSelfGlobalMouseUp(mouseDownEvent: MouseEvent, mouseUpEvent: JQueryMouseEventObject) {
    const xDelta = Math.abs(mouseDownEvent.clientX - mouseUpEvent.clientX);
    const yDelta = Math.abs(mouseDownEvent.clientY - mouseUpEvent.clientY);
    const didNotMoveMouse = xDelta <= MOUSE_MOVE_THRESHOLD && yDelta <= MOUSE_MOVE_THRESHOLD;

    if (!this.viewerState.drawingMode) {
      return true;
    }

    this.isDrawing = false;

    $(document).off(`mousemove.${this.uuid}`);
    $(document).off(`mouseup.${this.uuid}`);

    if (didNotMoveMouse) {
      return;
    }

    this.currentDrawing.endDraw();
    this.paintedLayer.draw();
  }

  private eventStartedFromIgnorableElement(event: MouseEvent): boolean {
    if (!event.target) {
      return false;
    }

    const $target = $(event.target);

    return $target.attr(PAGE_IGNORE_MOUSE_UP_ATTR) === 'true';
  }
}
