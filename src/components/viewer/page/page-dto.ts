export interface PageDto {
  guid: string;
  url: string;
}

export const PAGE_IGNORE_MOUSE_UP_ATTR = 'data-ignore-mup';
