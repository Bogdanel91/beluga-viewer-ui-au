import { ActionIdentity } from './../ribbon/action/action-types';
import { Color } from 'components/color-picker/color-picker-types';

interface CurrentColors {
  border: Color,
  fill: Color
}

export class ViewerState {
  drawingMode: ActionIdentity.DrawRectangle |
    ActionIdentity.DrawEllipse |
    ActionIdentity.DrawTextarea |
    ActionIdentity.DrawFree |
    ActionIdentity.EraseFree |
    null;
  drawingThickness: number;
  currentColors: CurrentColors;
}
