import { ActionIdentity } from 'components/ribbon/action/action-types';
import { ViewerCustomElement } from './viewer';

export class ViewerActionCallbacks {
  constructor(private readonly viewerCustomElement: ViewerCustomElement) {
    console.log(this.viewerCustomElement);
  }

  ZoomIn() {
    console.log(ActionIdentity.ZoomIn);
  }

  ZoomOut() {
    console.log(ActionIdentity.ZoomOut);
  }

  ResetZoom() {
    console.log(ActionIdentity.ResetZoom);
  }

  DrawRectangle() {
    console.log(ActionIdentity.DrawRectangle);
  }

  DrawEllipse() {
    console.log(ActionIdentity.DrawEllipse);
  }

  DrawTextarea() {
    console.log(ActionIdentity.DrawTextarea);
  }

  DrawFree() {
    console.log(ActionIdentity.DrawFree);
  }

  EraseFree() {
    console.log(ActionIdentity.EraseFree);
  }
}
