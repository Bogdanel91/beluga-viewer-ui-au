import { customElement } from 'aurelia-templating';
import { bindable, bindingMode } from 'aurelia-framework';
import { UiColor, Color } from 'components/color-picker/color-picker-types';

@customElement('blgz-color-picker')
export class ViewerThumbnailListCustomElement {
  @bindable colors: Array<Color> = [];
  @bindable({ defaultBindingMode: bindingMode.oneTime }) onSelect: (params?: any) => void;
  _colors: Array<UiColor> = [];


  bind() {
    this._colors = this.colors.map((color) => {
      return Object.assign({}, color, {
        isSelected: false
      })
    });
  }

  attached() {
    if (this._colors.length) {
      this.select(0);
    }
  }

  onSelfSelect(index: number, color: UiColor) {
    this.select(index, color);
  }

  select(index: number, color: UiColor = this._colors[0]) {
    if (this._colors[index]) {
      for (const color of this._colors) {
        color.isSelected = false;
      }

      this._colors[index].isSelected = true;
    }

    this.onSelect && this.onSelect({ index, color });
  }
}
