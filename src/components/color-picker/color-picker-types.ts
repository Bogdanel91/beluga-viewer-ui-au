export interface Color {
  name: string;
  code: string;
}

export interface UiColor extends Color {
  isSelected: boolean;
}
