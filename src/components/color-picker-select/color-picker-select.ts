import { Color } from 'components/color-picker/color-picker-types';
import 'spectrum-colorpicker';
import { customElement } from "aurelia-templating";
import { CommonHelper } from "helpers/common-helpers";
import { bindable, bindingMode } from 'aurelia-framework';

@customElement('blgz-color-picker-select')
export class ColorPickerSelectCustomElement {
  @bindable color: Color = {
    name: 'black',
    code: '#000000'
  };
  @bindable({ defaultBindingMode: bindingMode.oneTime }) onSelect: (data?: any) => void;
  @bindable({ defaultBindingMode: bindingMode.oneTime }) onMove: (data?: any) => void;
  uuid: string = CommonHelper.guid();
  inputElement: HTMLElement;
  $colorPicker: JQuery;

  attached() {
    this.$colorPicker = $(this.inputElement);
    this.$colorPicker.spectrum();
    this.$colorPicker.on('change.spectrum', this.onSelfSelect.bind(this));
    this.$colorPicker.on('move.spectrum', this.onSelfMove.bind(this));
  }

  detached() {
    this.$colorPicker.spectrum('destroy');
  }

  hide() {
    this.$colorPicker.spectrum('hide');
  }

  onSelfSelect() {
    this.onSelect && this.onSelect({ color: this.getColor() });
  }

  onSelfMove(_eventData: any, spectrumColor: tinycolorInstance) {
    this.onMove && this.onMove({ color: this.getColor(spectrumColor) });
  }

  getColor(spectrumColor?: tinycolorInstance): Color {
    const color = this.$colorPicker.spectrum('get');

    if (spectrumColor && spectrumColor.toHexString) {
      return {
        name: spectrumColor.toName(),
        code: spectrumColor.toHexString()
      };
    }

    return {
      name: color.toName(),
      code: color.toHexString()
    };
  }
}
