import { bindable } from 'aurelia-framework';
import { customElement } from "aurelia-templating";

@customElement('blgz-ribbon-group')
export class RibbonGroupCutomElement {
  @bindable name: string = '';
}
