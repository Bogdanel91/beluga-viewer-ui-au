export interface ThicknessUiItem {
  value: number;
  isSelected?: boolean;
}
