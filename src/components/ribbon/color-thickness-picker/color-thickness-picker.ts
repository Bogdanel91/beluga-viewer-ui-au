import { DomHelpers } from './../../../helpers/dom-helpers';
import { ActionIdentityData } from '../action/action-types';
import { customElement } from "aurelia-templating";
import { bindingMode, bindable, TaskQueue, inject } from "aurelia-framework";
import { ThicknessUiItem } from 'components/ribbon/color-thickness-picker/color-thickness-picker-types';
import { RibbonActionCutomElement } from 'components/ribbon/action/action';
import { ViewerState } from 'components/viewer/viewer-state';

const DEFAULT_VALUE = 6;
const FONT_SIZE_MODIFIER = 6;

@customElement('blgz-color-thickness-picker')
@inject(TaskQueue, ViewerState)
export class RibbonColorThicknessPickerCutomElement {
  @bindable({ defaultBindingMode: bindingMode.oneTime }) identityData: ActionIdentityData;
  @bindable({ defaultBindingMode: bindingMode.oneTime }) onClick: (params?: any) => void;
  name: string = 'Thickness';
  icon: string = 'circle';
  isPopoverVisible: boolean = false;
  actionVM: RibbonActionCutomElement;
  actionElement: Element;
  $actionElement: JQuery;
  fontSizeModifier: number = FONT_SIZE_MODIFIER;

  thickItems: Array<ThicknessUiItem> = [
    { value: 2 },
    { value: 4 },
    { value: 6 },
    { value: 8 },
    { value: 10 },
    { value: 12 }
  ];

  constructor(private readonly taskQueue: TaskQueue, private viewerState: ViewerState) {

  }

  attached() {
    this.$actionElement = $(this.actionElement);

    const idx = this.thickItems.findIndex(x => x.value === DEFAULT_VALUE);

    if (idx > -1) {
      this.select(idx);
    } else {
      this.select(0);
    }
  }

  select(index: number) {
    const len = this.thickItems.length;
    const fontSize = this.thickItems[index].value + this.fontSizeModifier;

    for (let i = 0; i < len; i++) {
      this.thickItems[i].isSelected = index === i;
    }

    this.viewerState.drawingThickness = fontSize - 2;

      this.taskQueue.queueTask(() => {
        DomHelpers.css(this.actionVM.iconElement, {
          'font-size': fontSize
        });
      });
  }

  onPopoverVisibilityChanged(isVisible: boolean) {
    this.actionVM.isActive = isVisible;
  }

  onSelfClick(event: Event) {
    this.onClick && this.onClick({
      event
    });

    this.isPopoverVisible = !this.isPopoverVisible;
  }

  onSelfThicknessSelect(_thickItem: ThicknessUiItem, index: number) {
    this.select(index);

    this.isPopoverVisible = false;
  }
}
