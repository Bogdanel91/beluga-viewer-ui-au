import { customElement } from "aurelia-templating";
import { bindable, bindingMode } from "aurelia-framework";
import { Color } from "components/color-picker/color-picker-types";
import { ColorPickerSelectCustomElement } from "components/color-picker-select/color-picker-select";

@customElement('blgz-color-picker-select-ribbon')
export class ColorPickerSelectRibbonCustomElement {
  @bindable name: string = '';
  @bindable color: Color;
  @bindable({ defaultBindingMode: bindingMode.oneTime }) onSelect: (data?: any) => void;
  @bindable({ defaultBindingMode: bindingMode.oneTime }) onMove: (data?: any) => void;
  colorPickerSelectVM: ColorPickerSelectCustomElement;
  timeoutId: number;

  hide() {
    this.colorPickerSelectVM.hide();
  }

  onSelfSelect(color: Color) {
    this.onSelect && this.onSelect({ color });
  }

  onSelfMove(color: Color) {
    if (!this.onSelect) {
      return;
    }

    clearTimeout(this.timeoutId);
    this.timeoutId = window.setTimeout(() => {
      this.onSelect({ color });
    }, 1);
  }
}
