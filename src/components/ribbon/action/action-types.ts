export enum ActionToggleGroup {
  Drawing = 'Drawing'
}

export enum ActionIdentity {
  ZoomIn = 'ZoomIn',
  ZoomOut = 'ZoomOut',
  ResetZoom = 'ResetZoom',
  DrawFree = 'DrawFree',
  DrawRectangle = 'DrawRectangle',
  DrawEllipse = 'DrawEllipse',
  DrawTextarea = 'DrawTextarea',
  EraseFree = 'EraseFree',
  ColorAndThickness = 'ColorAndThickness'
}

export interface ActionIdentityData {
  identity: ActionIdentity;
  toggleGroup?: ActionToggleGroup;
}
