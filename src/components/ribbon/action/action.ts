import { ComponentHelpers } from './../../../helpers/component-helpers';
import { customElement } from "aurelia-templating";
import { bindable, bindingMode } from "aurelia-framework";
import { ActionIdentityData } from 'components/ribbon/action/action-types';

const COMPONENT_NAME = 'blgz-ribbon-action';
const EVENT_CLICKED = [COMPONENT_NAME, '-clicked'].join('');

@customElement(COMPONENT_NAME)
export class RibbonActionCutomElement {
  @bindable name: string = '';
  @bindable icon: string = '';
  @bindable({ defaultBindingMode: bindingMode.oneTime }) identityData: ActionIdentityData;
  @bindable({ defaultBindingMode: bindingMode.oneTime }) onClick: (params?: any) => void;
  isActive: boolean;
  actionElement: Element;
  iconElement: Element;

  onSelfClick(event: Event) {
    this.onClick && this.onClick({
      event
    });

    ComponentHelpers.dispatch(this.actionElement, EVENT_CLICKED, this);
  }
}
