import { ViewerCustomElement } from 'components/viewer/viewer';
import { PageDto } from "components/viewer/page/page-dto";
import { inject, Aurelia } from 'aurelia-framework';
// import { CommonHelper } from 'helpers/common-helpers';

@inject(Aurelia)
export class Welcome {
  viewerVM: ViewerCustomElement;

  constructor(private aurelia: Aurelia) { }

  attached() {
    this.aurelia.use.instance('appApi', this.viewerVM);
  }

  pages: Array<PageDto> = [
    // { guid: CommonHelper.guid(), url: 'http://i.imgur.com/SMcRxfp.jpg' },
    // { guid: CommonHelper.guid(), url: 'http://i.imgur.com/47RfK5w.jpg' },
    // { guid: CommonHelper.guid(), url: 'http://i.imgur.com/xB48VvO.jpg' },
    // { guid: CommonHelper.guid(), url: 'http://i.imgur.com/OVvR7eA.jpg' },
    // { guid: CommonHelper.guid(), url: 'http://i.imgur.com/ZLP2dZ0.jpg' }
  ]
}
