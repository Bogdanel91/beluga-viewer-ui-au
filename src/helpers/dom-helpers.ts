export class DomHelpers {
  static scrollTo(element: Element, container: Element, isAnimated: boolean = true) {
    const $containerElement = $(container);
    const $containedElement = $(element);
    if ($containerElement[0] && $containedElement[0]) {
      const containerScrollTop = $containerElement.scrollTop()!;
      const containerTop = $containerElement.offset()!.top;
      const containedTop = $containedElement.offset()!.top;
      const containerPaddingTop = parseInt($containerElement.css('padding-top'), 10);
      const containerMarginTop = parseInt($containerElement.css('margin-top'), 10);

      if (isAnimated) {
        $containerElement
          .stop()
          .animate({
            scrollTop: containerScrollTop + containedTop - containerTop - containerPaddingTop - containerMarginTop
          });
      } else {
        $containerElement
          .stop()
          .scrollTop(containerScrollTop + containedTop - containerTop - containerPaddingTop - containerMarginTop);
      }
    }
  }

  static addClass(element: Element, className: string) {
    if (element.classList) {
      element.classList.add(className);
    } else {
      element.className += ' ' + className;
    }
  }

  static removeClass(element: Element, className: string) {
    if (element.classList) {
      element.classList.remove(className);
    } else {
      element.className = element.className.replace(new RegExp('(^|\\b)' + className.split(' ').join('|') + '(\\b|$)', 'gi'), ' ');
    }
  }

  static css(element: Element, rules: { [key: string]: any }) {
    return $(element).css(rules);
  }
}
