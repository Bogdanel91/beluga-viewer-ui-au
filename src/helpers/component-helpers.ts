import { DOM } from 'aurelia-pal';

export class ComponentHelpers {
  static dispatch(element: Element, event: string, data?: any) {
    element.dispatchEvent(DOM.createCustomEvent(event, {
      bubbles: true,
      detail: data
    }))
  }
}
