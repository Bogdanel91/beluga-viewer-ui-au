export enum Keyboard {
  Backspace = 8,
  Tab = 9,
  F = 70,
  Enter = 13,
  Esc = 27,
  Delete = 46,
  SemiColon = 186,
  Comma = 188,
  Space = 32,
  F2 = 113
}
