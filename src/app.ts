import { Router, RouterConfiguration } from 'aurelia-router';
import { PLATFORM } from 'aurelia-pal';
import { inject, Aurelia } from 'aurelia-framework';
import { ViewerCustomElement } from 'components/viewer/viewer';

@inject(Aurelia)
export class App {
  router: Router;

  constructor(public aurelia: Aurelia) { }

  get appApi(): ViewerCustomElement {
    return this.aurelia.container.get('appApi');
  }

  configureRouter(config: RouterConfiguration, router: Router) {
    config.title = 'Aurelia';
    config.map([
      { route: ['', 'welcome'], name: 'welcome', moduleId: PLATFORM.moduleName('./welcome'), nav: true, title: 'Welcome' }
    ]);

    this.router = router;
  }
}
